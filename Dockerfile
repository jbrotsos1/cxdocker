# CheckMarx Console requires JRE to run. Our image is based on light alpine image with JRE
FROM openjdk:jre-alpine

# Latest CxCli download url (needs to be updated)
ARG CHECKMARX_CLI_URL="https://download.checkmarx.com/9.0.0/Plugins/CxConsolePlugin-9.00.2.zip"

# Directory name of the console plugin (needs to be updated)
ARG CHECKMARX_DIRECTORY="CxConsolePlugin-9.00.2"


# Libraries to download CxConsole & process API calls
RUN apk add --no-cache --update curl python jq bash libxml2-utils maven

RUN curl ${CHECKMARX_CLI_URL} -o /tmp/console.zip

RUN mkdir -p /opt/CxConsolePlugin && \
    mkdir -p /tmp/${CHECKMARX_DIRECTORY} && \
    unzip /tmp/console.zip -d /tmp/${CHECKMARX_DIRECTORY} && \
    mv /tmp/${CHECKMARX_DIRECTORY}/* /opt/CxConsolePlugin && \
    rm -rf /tmp/console.zip && \
    rm -rf /tmp/${CHECKMARX_DIRECTORY} && \
    chmod +x /opt/CxConsolePlugin/runCxConsole.sh && \
    rm -rf /var/cache/apk/*

RUN chmod 0777 /opt/CxConsolePlugin/runCxConsole.sh

ENV PATH="/opt/CxConsolePlugin:${PATH}"
